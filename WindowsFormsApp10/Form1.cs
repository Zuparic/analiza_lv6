﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp10
{
    public partial class Kalkulator : Form
    {
        double x, y, rez;
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            if (!double.TryParse(tB2.Text, out y))
            {
                MessageBox.Show("Krivi unos drugog člana!");
            }
            rez = x + y;
            tB_Rezultat.Text = rez.ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            if (!double.TryParse(tB2.Text, out y))
            {
                MessageBox.Show("Krivi unos drugog člana!");
            }
            rez = x / y;
            tB_Rezultat.Text = rez.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            if (!double.TryParse(tB2.Text, out y))
            {
                MessageBox.Show("Krivi unos drugog člana!");
            }
            rez = x - y;
            tB_Rezultat.Text = rez.ToString();
        }

        private void tb2_TextChanged(object sender, EventArgs e)
        {

        }

        private void bt_Mnozi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            if (!double.TryParse(tB2.Text, out y))
            {
                MessageBox.Show("Krivi unos drugog člana!");
            }
            rez = x * y;
            tB_Rezultat.Text = rez.ToString();
        }

        private void bt_Sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            
            rez = Math.Sin(x);
            tB_Rezultat.Text = rez.ToString();

        }

        private void bt_Cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            
            rez = Math.Cos(x);
            tB_Rezultat.Text = rez.ToString();
        }

        private void bt_Log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            
            rez = Math.Log(x);
            tB_Rezultat.Text = rez.ToString();
        }

        private void bt_Sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog člana!");
            }
            
            rez = Math.Sqrt(x);
            tB_Rezultat.Text = rez.ToString();
        }

        private void tB_Rezultat_TextChanged(object sender, EventArgs e)
        {
            tB_Rezultat.Text = rez.ToString();
            tB_Rezultat.Show();
            tB1.Clear();
            tB2.Clear();
        }

        private void tB1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
